import React, {useContext} from "react";
import {Route, Redirect} from "react-router-dom";
import {LoginHeaderContext} from "../../context/loginHeaderContext";

function PrivateRoute({component: Component, ...rest}) {
	const [getData, isLogin] = useContext(LoginHeaderContext);
	return (
		<Route
			{...rest}
			render={(props) =>
				isLogin ? (
					<Component {...props} />
				) : (
					<Redirect
						to={{
							pathname: "/",
						}}
					/>
				)
			}
		/>
	);
}
export default PrivateRoute;
