import React from "react";
import StyledFirebaseAuth from "react-firebaseui/StyledFirebaseAuth";
import app from "../../firebase/fireBase";
import firebase from "firebase/app";

const uiConfig = {
	// Popup signin flow rather than redirect flow.
	signInFlow: "popup",
	// Redirect to /signedIn after sign in is successful. Alternatively you can provide a callbacks.signInSuccess function.
	signInSuccessUrl: "/profile",
	// We will display Google and Facebook as auth providers.
	signInOptions: [firebase.auth.GoogleAuthProvider.PROVIDER_ID],
};

export function GoogleSignIn() {
	return <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={app.auth()} />;
}
