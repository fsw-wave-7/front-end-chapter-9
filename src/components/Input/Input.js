import React from 'react'

export default function inputRegister(props) {
    return (
        <div>
            <label>
                <strong>{props.labelFor}</strong>
            </label>
            <br />
            <input 
                type        = {props.type} 
                value       = {props.value} 
                onChange    = {props.change} 
                name        = {props.name} 
                placeholder = {`Masukkan ${props.name}`}>
            </input>
        </div>
    )
}
