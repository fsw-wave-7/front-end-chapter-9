import React, { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { nav, auth } from './HeaderComponent.module.css';
import { LoginHeaderContext } from '../../context/loginHeaderContext';

export default function HeaderComponent() {
  // -----GET CONTEXT
  const [getData, userEmail] = useContext(LoginHeaderContext);
  return (
    <Navbar
      className={`${nav} d-flex align-items-center justify-content-between`}
      expand="lg"
      variant="dark"
      sticky="top"
      collapseOnSelect
    >
      <Navbar.Brand>
        <h1 className="my-0">
          TEAM <strong>FSW 7</strong>
        </h1>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav" className="text-right">
        <Nav className="mr-md-5">
          <Nav.Link className="mr-md-3">
            <Link to="/">Home</Link>
          </Nav.Link>
          <Nav.Link>
            <Link to="/game">Games</Link>
          </Nav.Link>
        </Nav>
        <Nav className={auth}>
          {userEmail ? (
            <>
              <Nav.Link>
                <Link exact to="/profile">
                  Profil
                </Link>
              </Nav.Link>
              <Nav.Link>
                <Link exact to="/">
                  Log Out
                </Link>
              </Nav.Link>
            </>
          ) : (
            <>
              <Nav.Link className="mr-md-3">
                <Link to="/register">Register</Link>
              </Nav.Link>
              <Nav.Link>
                <Link to="/login">Login</Link>
              </Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}
