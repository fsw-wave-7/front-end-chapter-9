import React from 'react';
import { Container, Table } from 'react-bootstrap';
import styles from './profileComponent.module.css';
import { Link } from 'react-router-dom';

export default function Userprofilecomponent({ userData }) {
  console.log(userData);
  return (
    <Container className={styles.container}>
      <h1 className={`${styles.mobileCenter} mt-5`}>Your Profile</h1>
      <div className="row d-flex">
        <div className={`${styles.mobileCenter} col-sm-md-lg-6`}>
          <img
            className="mx-3"
            alt="gambar profile"
            src="https://pbs.twimg.com/media/DhsRKlhUEAAc50O.jpg"
          />
        </div>
        <div className={`${styles.mobileCenter} col-sm-md-lg-6`}>
          <Table responsive borderless className="px-3">
            <tbody>
              {userData.map((user, index) => {
                return (
                  <>
                    <tr key={index}>
                      <td>Username</td>
                      <td>{user.username}</td>
                    </tr>
                    <tr>
                      <td>Email</td>
                      <td>{user.email}</td>
                    </tr>
                    <tr>
                      <td>Biodata</td>
                      <td>{user.bio}</td>
                    </tr>
                    <tr>
                      <td>Sosmed</td>
                      <td>{user.sosmed}</td>
                    </tr>
                    <tr>
                      <td>Location</td>
                      <td>{user.city}</td>
                    </tr>
                    <tr>
                      <td>
                        <Link to="/userupdate" className="" onClick="">
                          <button class="btn btn-outline-info">Update</button>
                        </Link>
                      </td>
                    </tr>
                  </>
                );
              })}
            </tbody>
          </Table>
        </div>
      </div>
    </Container>
  );
}
