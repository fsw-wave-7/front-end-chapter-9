import React from 'react';
import { Container, Table } from 'react-bootstrap';

export default function Userlist({ allUserData }) {
  console.log(allUserData);
  return (
    <Container>
      {/* =============== ALL USER PROFILE =====================*/}
      <h1 className="mt-5">All User Profile</h1>
      <Table hover responsive>
        <thead>
          <tr>
            <th>No</th>
            <th>Username</th>
            <th>Email</th>
            <th>Location</th>
            <th>Biodata</th>
            <th>Sosmed</th>
          </tr>
        </thead>
        <tbody>
          {allUserData.map((user, index) => {
            return (
              <>
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{user.username}</td>
                  <td>{user.email}</td>
                  <td>{user.city}</td>
                  <td>{user.bio}</td>
                  <td>{user.sosmed}</td>
                </tr>
              </>
            );
          })}
        </tbody>
      </Table>
    </Container>
  );
}
