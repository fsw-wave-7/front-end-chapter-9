import CONFIG from "./config";

const API_ENDPOINT = {
	REGISTER: `${CONFIG.BASE_URL}/register`,
	LOGIN: `${CONFIG.BASE_URL}/login`,
	GET_ALL_USERS: `${CONFIG.BASE_URL}/user`,
	GET_ALL_GAMES: `${CONFIG.BASE_URL}/game`,
	GET_DETAIL_USERS: (id) => `${CONFIG.BASE_URL}/user/${id}`,
	ADD_SCORE: (id) => `${CONFIG.BASE_URL}/score/${id}`,
	CLICK_COUNT: `${CONFIG.BASE_URL}/play/1`,
};

export default API_ENDPOINT;
