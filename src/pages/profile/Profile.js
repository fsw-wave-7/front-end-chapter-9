import React, { Component, Fragment } from 'react';
import UserListComponent from '../../components/listUserComponent/ListUserComponent';
import UserProfileComponent from '../../components/profileComponent/ProfileComponent';
import GameSource from '../../heroku/data/game-source';
import { AuthContext } from '../../context/authContext';

export default class UserProfile extends Component {
  static contextType = AuthContext;
  state = {
    isLoading: true,
    username: null,
    currentUser: null,
    userData: [],
    allUserData: [],
  };

  async componentDidMount() {
    document.querySelector('.navbar').style.backgroundColor = 'black';
    const data = await GameSource.usersList();
    const [signUp, login, isSignUp, user, isLogin] = await this.context;
    const dataUserLogin = await data.filter(
      (userLogin) => userLogin.email === user.email
    );
    console.log(dataUserLogin);
    this.setState({ allUserData: data, userData: dataUserLogin });
  }

  get Loader() {
    return <em>Loading...</em>;
  }

  render() {
    return (
      <Fragment>
        <UserProfileComponent userData={this.state.userData} />
        <UserListComponent allUserData={this.state.allUserData} />
      </Fragment>
    );
  }
}
