import React, {useContext} from "react";
import {Link} from "react-router-dom";
import {Container, Row, Col, Button} from "react-bootstrap";
import imgrps from "../../../src/assets/images/rps.png";
import rock from "../../../src/assets/images/rock.png";
import paper from "../../../src/assets/images/paper.png";
import scissors from "../../../src/assets/images/scissors.png";
import {GameContext} from "../../context/gameContext";
import "./GameRockPaperScissors.css";

const RockPaperScissors = () => {
	const [setMyChoice, , ,score] = useContext(GameContext);
	const setChoice = (e) => {
		setMyChoice(e.target.dataset.id);
	};

	return (
		<div className="rps background-game">
			<Container>
				<Row>
					<Col xs={1} className="px-2 backrps">
						<Link to="/game">
							<Button variant="primary">BACK</Button>
						</Link>
					</Col>
					<Col xs={1} className="px-2 py-4 ml-4">
						<img src={imgrps} fluid />
					</Col>
					<Col xs={4} className="px-2 py-4 judulrps">
						<h2>ROCK PAPER SCISSORS</h2>
					</Col>
				</Row>
				<Row>
					<Col>
						<h3>Score : {score}</h3>
					</Col>
				</Row>
				<Row>
					<Col>
						My Choice: <br />
					</Col>
				</Row>
				<Row>
					<Col>
						<Link to="/game/rock-paper-scissors-result">
							<div className="py-4">
								<img src={rock} height="75" data-id="rock" onClick={setChoice} className="icon icon--rock" alt="imgrock" />
							</div>
						</Link>
					</Col>
				</Row>

				<Row>
					<Col>
						<Link to="/game/rock-paper-scissors-result">
							<div className="py-4">
								<img src={paper} height="75" data-id="paper" onClick={setChoice} className="icon icon--paper" alt="imgpaper" />
							</div>
						</Link>
					</Col>
				</Row>

				<Row>
					<Col>
						<Link to="/game/rock-paper-scissors-result">
							<div className="py-4">
								<img src={scissors} height="75" data-id="scissors" onClick={setChoice} className="icon icon--scissors" alt="imgscissors" />
							</div>
						</Link>
					</Col>
				</Row>
			</Container>
		</div>
	);
};

export default RockPaperScissors;
