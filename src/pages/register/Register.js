import React, {useContext, useRef, useState} from "react";
import Footer from "../../components/FooterComponent/FooterComponent";
// Context
import {AuthContext} from "../../context/authContext";
// Bootstrap
import {Card, Form, Button} from "react-bootstrap";
// import CSS
import styles from "./register.module.css";
// Route
import {useHistory} from "react-router-dom";
// API ENDPOINT
import API_ENDPOINT from "../../heroku/api-endpoint";

function Register() {
	// -------GET CONTEXTS
	const [signUp, login, isSignUp, user, isLog] = useContext(AuthContext);
	// ------CREATE STATE
	const [loadingColor, setLoadingColor] = useState(styles.button);
	const [loadingText, setLoadingText] = useState("Register");
	const [disabled, setDisabled] = useState("");
	// -------GET VALUE FROM FORM INPUT
	const emailRef = useRef();
	const passRef = useRef();
	const userRef = useRef();
	const history = useHistory();

	async function handleSubmit(e) {
		// ------LOADING SETTING
		setLoadingColor(styles.loading);
		setLoadingText("Loading");
		setDisabled("disabled");

		// -----PREVENT FROM REFRESHING
		e.preventDefault();

		// -------REGISTER FUNCTION FROM FIREBASE
		await signUp(emailRef.current.value, passRef.current.value);

		// --------FETCHING API FOR REGISTER
		let a = await fetch(API_ENDPOINT.REGISTER, {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				username: userRef.current.value,
				email: emailRef.current.value,
				password: passRef.current.value,
			}),
		});

		console.log(a);

		console.log("after click =>" + isSignUp);

		if (isSignUp) {
			alert("Register Success!!");
			history.push("/login");
		} else {
			alert("Register Failure!!");
			history.push("/register");
		}
	}

	return (
		<>
			<div className={`${styles.Register} d-flex align-items-center justify-content-center`}>
				<Card
					style={{
						maxWidth: "600px",
						minWidth: "400px",
						borderRadius: "5px",
						backgroundColor: "#ffffff2e",
					}}
					className={`d-flex justify-content-center`}
				>
					<Card.Body className="w-100">
						<h1 className="mb-3">Register</h1>
						<Form onSubmit={handleSubmit}>
							<Form.Group id="username">
								<Form.Label>Username</Form.Label>
								<Form.Control ref={userRef} type="text"></Form.Control>
							</Form.Group>
							<Form.Group id="email">
								<Form.Label>Email</Form.Label>
								<Form.Control ref={emailRef} type="email"></Form.Control>
							</Form.Group>
							<Form.Group id="password">
								<Form.Label>Password</Form.Label>
								<Form.Control ref={passRef} type="password"></Form.Control>
							</Form.Group>
							<button className={`${loadingColor} ${disabled} btn`} type="submit" value="submit">
								{loadingText}
							</button>
						</Form>
						<div class="w-100 text-center mt-3">
							Already have an account ?{" "}
							<a className={styles.login} href="/login">
								Log In
							</a>
						</div>
					</Card.Body>
				</Card>
			</div>
			<Footer />
		</>
	);
}

export default Register;
