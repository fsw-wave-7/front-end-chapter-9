import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import HeaderComponent from './components/HeaderComponent/HeaderComponent';
import LandingPage from './pages/landingPage/LandingPage';
import GameLists from './pages/landingPage/gameLists/GameLists';
import Register from './pages/register/Register';
import Login from './pages/login/Login';
// import Update from './pages/update/Update';
import ProtectedRoutes from './components/ProtectedRoute/ProtectedRoutes';
import 'bootstrap/dist/css/bootstrap.min.css';

import React, { useState } from 'react';
import RockPaperScissors from './pages/GameRockPaperScissors/GameRockPaperScissors';
import RockPaperScissorsResult from './pages/GameRockPaperScissorsResult/GameRockPaperScissorsResult';
import Profile from './pages/profile/Profile';
import { AuthContextProvider } from './context/authContext';
import LoginHeaderProvider from './context/loginHeaderContext';
import { GameContextProvider } from './context/gameContext';

function App() {
  // const [myChoice, setMyChoice] = useState("");

  // const [score, setScore] = useState("");

  return (
    <AuthContextProvider>
      <LoginHeaderProvider>
        <GameContextProvider>
          <div className="App">
            <BrowserRouter>
              <HeaderComponent />
              <Switch>
                <Route exact path="/" component={LandingPage} />
                <Route exact path="/game" component={GameLists} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                {/* <Route exact path="/userupdate" component={Update} /> */}
                <ProtectedRoutes exact path="/profile" component={Profile} />
                <ProtectedRoutes
                  exact
                  path="/game/rock-paper-scissors"
                  component={RockPaperScissors}
                />

                {/* <Route path="/game/rock-paper-scissors">
									<RockPaperScissors score={score} setMyChoice={setMyChoice} />
									<RockPaperScissors />
								</Route> */}

                <Route path="/game/rock-paper-scissors-result">
                  {/* <RockPaperScissorsResult myChoice={myChoice} score={score} setScore={setScore} /> */}
                  <RockPaperScissorsResult />
                </Route>
              </Switch>
            </BrowserRouter>
          </div>
        </GameContextProvider>
      </LoginHeaderProvider>
    </AuthContextProvider>
  );
}

export default App;
