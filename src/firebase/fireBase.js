import firebase from "firebase/app";
import "firebase/auth";

let app = firebase.initializeApp({
	apiKey: "AIzaSyCDjoO1Rr3DkrvFdE8XkGhMzSR7Z1VPCAA",
	authDomain: "auth-development-2fe85.firebaseapp.com",
	projectId: "auth-development-2fe85",
	storageBucket: "auth-development-2fe85.appspot.com",
	messagingSenderId: "1072500249908",
	appId: "1:1072500249908:web:8d0ab30f2e7b4ac08f8161",
});

export const auth = app.auth();

export default app;
