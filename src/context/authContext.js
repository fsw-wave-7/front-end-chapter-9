import React from "react";
import {auth} from "../firebase/fireBase";
export const AuthContext = React.createContext();

export function AuthContextProvider({children}) {
	const [isSignUp, setIsSignUp] = React.useState(false);
	const [user, setUser] = React.useState(null);
	const [isLogin, setIsLogin] = React.useState(false);

	React.useEffect(() => {
		auth.onAuthStateChanged((data) => {
			data.email !== null ? setIsSignUp(true) : setIsSignUp(false);
			setUser(data);
			console.log("before click => " + user);
			console.log("before click => " + isSignUp);
		});
	}, []);

	function signUp(email, password) {
		return auth.createUserWithEmailAndPassword(email, password);
	}

	function login(email, password) {
		setIsLogin(true);
		return auth.signInWithEmailAndPassword(email, password);
	}

	// function logOut() {
	// 	auth.signOut()
	// 		.then(() => {
	// 			setUser(null);
	// 		})
	// 		.catch((error) => {
	// 			console.log(error);
	// 		});
	// }

	return <AuthContext.Provider value={[signUp, login, isSignUp, user, isLogin]}>{children}</AuthContext.Provider>;
}
