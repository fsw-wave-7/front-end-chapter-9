import React, {useState, useContext} from "react";
// API ENDPOINT
import API_ENDPOINT from "../heroku/api-endpoint";
import GameSource from "../heroku/data/game-source";
import {AuthContext} from "../context/authContext";

export const GameContext = React.createContext();

export function GameContextProvider({children}) {
	const [signUp, login, , user] = useContext(AuthContext);
	let [myChoice, setMyChoice] = useState(0);

	let [score, setScore] = useState(0);

	const setChoice = (e) => {
		setMyChoice(e);
	};

	async function scoring(e) {
		setScore(e);
		let getData = await fetch(API_ENDPOINT.GET_ALL_USERS);
		let dataJson = await getData.json();
		// console.log(dataJson);

		// // --------FETCHING API FOR REGISTER
		let addScore = await fetch(API_ENDPOINT.ADD_SCORE(dataJson.data.find((x) => x.email === user.email).id), {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				total_score: score,
			}),
		});
	}
	return <GameContext.Provider value={[setChoice, myChoice, scoring, score]}>{children}</GameContext.Provider>;
}
